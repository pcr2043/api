<?php

namespace App\Logic;

use App\User;
use Illuminate\Support\Facades\Validator;

class Users{

    public function list(){

        return User::all();

    }

    public function create($data){


        // Validate data

        $rules = [
            'name' => 'string|required',
            'email' => 'required|email|unique:users',
            'birthdate' =>'required|date',
            'gender' => 'required|numeric|min:0|max:1'
        ];

        // Valite the Request
        $validatedData = Validator::make($data, $rules)->validate();


        try{

            // Create a new User instance, full and save
            $user = new User();
            $user->fill($validatedData);
            $user->save();

            // return response with the user
            return ['result' => true, 'msg' => 'user created', 'data' => $user];

        }
        catch(\Exception $ex)
        {
            return ['result' => false, 'msg' => $ex->getMessage()];
        }


    }

    public function update($data, $user){


        // Validate data

        $rules = [
            'name' => 'string',
            'email' => 'email|unique:users.'.$user->id,
            'birthdate' =>'date',
            'gender' => 'numeric|min:0|max:1'
        ];

        // Valite the Request
        $validatedData = Validator::make($data, $rules)->validate();


        try{

            // fill the user with the new data
            $user->fill($validatedData);
            $user->save();

            // return response with the user
            return ['result' => true, 'msg' => 'user saved', 'data' => $user];

        }
        catch(\Exception $ex)
        {
            return ['result' => false, 'msg' => $ex->getMessage()];
        }


    }



    public function removeById($id){


        try{
            
            // call method destroy on user
            User::destroy($id);

            // return response with the user
            return ['result' => true, 'msg' => 'user deleted', 'data' => $id];

        }
            catch(\Exception $ex)
        {
            return ['result' => false, 'msg' => $ex->getMessage()];
        }
    }

    
}