<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class APITest extends TestCase
{
    /**
     * Test Api base endpoint and users endpoit .
     *
     * @return void
     */
    public function testEndpoint()
    {
        $response = $this->get('/api/');

        $response->assertStatus(200);
    }

    public function testUsersList()
    {
        $response = $this->get('/api/users');

        $response->assertStatus(200)->assertJsonCount(User::count());
      
    }


    public function testAddUser()
    {
        $response = $this->json('POST', '/api/users', [
            'name' => 'Sally', 
            'email' => 'teste@sapo.pt'.User::count(), 
            'birthdate' => '12/12/2000', 
            'gender' => 1]);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'result' => true,
            ]);
      
    }

}
